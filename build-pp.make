api = 2
core = 7.x

projects[drupal][version] = "7.26"
; This fixes multiple upload fields in pp_media_library. http://drupal.org/node/1620030
projects[drupal][patch][1620030] = http://drupal.org/files/1620030-d7-2.patch

; Installation profile
projects[pp][type] = "profile"
projects[pp][download][type] = git
projects[pp][download][branch] = "7.x-1.x"
