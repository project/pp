; Drush make api version.
api = 2
core = 7.23

; Contrib Modules

projects[job_scheduler][subdir] = "contrib"
projects[job_scheduler][version] = "2.0-alpha3"

projects[feeds][subdir] = "contrib"
projects[feeds][version] = "2.0-alpha8"

projects[sharethis][subdir] = "contrib"
projects[sharethis][version] = "2.5"

projects[devel][subdir] = "devel"
projects[devel][version] = "1.4"

projects[diff][subdir] = "devel"
projects[diff][version] = "3.2"

projects[workbench][subdir] = "contrib"
projects[workbench][version] = "1.2"

projects[workbench_moderation][subdir] = "contrib"
projects[workbench_moderation][branch] = "7.x-1.x"
projects[workbench_moderation][revision] = a90378d
projects[workbench_moderation][patch][1285090] = "https://drupal.org/files/playnicewithpanels-1285090-22.patch"

projects[edit][subdir] = "contrib"
projects[edit][version] = "1.0-alpha11"

; Theme.

projects[skeletontheme][type] = "theme"
projects[skeletontheme][version] = "1.3"

projects[shiny][type] = "theme"
projects[shiny][version] = "1.3"

; Custom modules.

projects[pp_core][type] = "module"
projects[pp_core][subdir] = "custom"
projects[pp_core][download][type] = "git"

projects[pp_admin][type] = "module"
projects[pp_admin][subdir] = "custom"
projects[pp_admin][download][type] = "git"

projects[pp_crop][type] = "module"
projects[pp_crop][subdir] = "custom"
projects[pp_crop][download][type] = "git"

projects[pp_lightbox][type] = "module"
projects[pp_lightbox][subdir] = "custom"
projects[pp_lightbox][download][type] = "git"

projects[pp_wysiwyg][type] = "module"
projects[pp_wysiwyg][subdir] = "custom"
projects[pp_wysiwyg][download][type] = "git"

projects[pp_slideshow][type] = "module"
projects[pp_slideshow][subdir] = "custom"
projects[pp_slideshow][download][type] = "git"

projects[pp_socials][type] = "module"
projects[pp_socials][subdir] = "custom"
projects[pp_socials][download][type] = "git"
projects[pp_socials][download][url] = "http://git.drupal.org/sandbox/m1r1k/2156599.git"

projects[pp_seo][type] = "module"
projects[pp_seo][subdir] = "custom"
projects[pp_seo][download][type] = "git"
projects[pp_seo][download][url] = "http://git.drupal.org/sandbox/m1r1k/2156623.git"
projects[pp_seo][download][branch] = 7.x-1.x

projects[pp_media_library][type] = "module"
projects[pp_media_library][subdir] = "custom"
projects[pp_media_library][download][type] = "git"
projects[pp_media_library][download][url] = "http://git.drupal.org/sandbox/asgorobets/2156787.git"
projects[pp_media_library][download][branch] = 7.x-1.x

projects[pp_accordion_style][type] = "module"
projects[pp_accordion_style][subdir] = "features"
projects[pp_accordion_style][download][type] = "git"
projects[pp_accordion_style][download][url] = "http://git.drupal.org/sandbox/undertext/2156629.git"

projects[pp_news][type] = "module"
projects[pp_news][subdir] = "features"
projects[pp_news][download][type] = "git"

projects[bean_wysiwyg][type] = "module"
projects[bean_wysiwyg][subdir] = "contrib"

projects[pp_demo_beans][type] = "module"
projects[pp_demo_beans][subdir] = "features"
projects[pp_demo_beans][download][type] = "git"

; Framework for demo content.
projects[pp_demo][type] = "module"
projects[pp_demo][subdir] = "features"
projects[pp_demo][download][type] = "git"

projects[panopoly_magic][subdir] = "contrib"
projects[panopoly_magic][version] = "1.0-rc5"

projects[fape][subdir] = "contrib"
projects[fape][version] = "1.1"

projects[pp_panopoly_magic_settings][type] = "module"
projects[pp_panopoly_magic_settings][subdir] = "custom"
projects[pp_panopoly_magic_settings][download][type] = "git"
projects[pp_panopoly_magic_settings][download][url] = "http://git.drupal.org/sandbox/32i/2153049.git"
